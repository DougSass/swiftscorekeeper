//
//  ViewController.swift
//  SwiftScoreKeeper
//
//  Created by Douglas Sass on 2/2/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var textTeam1 = UITextField()
    var textTeam2 = UITextField()
    
    var lblTeam1 = UILabel()
    var lblTeam2 = UILabel()
    
    var stepperTeam1 = UIStepper()
    var stepperTeam2 = UIStepper()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Text field for the Team 1 Name
        textTeam1 = UITextField(frame: CGRect(x: 10, y: 75, width: 180, height: 45))
        textTeam1.placeholder = "Team 1 Name"
        textTeam1.textAlignment = NSTextAlignment.center
        textTeam1.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(textTeam1)
        
        // Label for team 1 where it keeps the count of score
        lblTeam1 =  UILabel(frame: CGRect(x: 60, y: 150, width: 80, height: 75))
        lblTeam1.text = "0"
        lblTeam1.textAlignment = NSTextAlignment.center
        lblTeam1.font = UIFont.systemFont(ofSize: 50)
        lblTeam1.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(lblTeam1)
        
        // Stepper for team 1 that is used to increase and decrease the score in label for team 1
        stepperTeam1 = UIStepper(frame: CGRect(x: 53, y: 250, width: 0, height: 0))
        stepperTeam1.minimumValue = 0
        stepperTeam1.maximumValue = 21
        stepperTeam1.value = 0
        stepperTeam1.autoresizingMask = UIViewAutoresizing.flexibleLeftMargin
        self.view.addSubview(stepperTeam1)
        stepperTeam1.addTarget(self, action: #selector(team1StepperTouched(stepper:)), for: UIControlEvents.valueChanged)
        
        
        // Team 2
        // Text field for the Team 2 Name
        textTeam2 = UITextField(frame: CGRect(x: 210, y: 75, width: 180, height: 45))
        textTeam2.placeholder = "Team 2 Name"
        textTeam2.textAlignment = NSTextAlignment.center
        textTeam2.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(textTeam2)
        
        // Label for team 2 where it keeps the count of score
        lblTeam2 =  UILabel(frame: CGRect(x: 260, y: 150, width: 80, height: 75))
        lblTeam2.text = "0"
        lblTeam2.textAlignment = NSTextAlignment.center
        lblTeam2.font = UIFont.systemFont(ofSize: 50)
        lblTeam2.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(lblTeam2)
        
        // Stepper for team 2 that is used to increase and decrease the score in label for team 2
        stepperTeam2 = UIStepper(frame: CGRect(x: 253, y: 250, width: 0, height: 0))
        stepperTeam2.minimumValue = 0
        stepperTeam2.maximumValue = 21
        stepperTeam2.value = 0
        stepperTeam2.autoresizingMask = UIViewAutoresizing.flexibleRightMargin
        self.view.addSubview(stepperTeam2)
        stepperTeam2.addTarget(self, action: #selector(team2StepperTouched(stepper:)), for: UIControlEvents.valueChanged)
        
        
        // Reset button returns all team labels and steppers to 0
        let btnReset = UIButton(type: UIButtonType.system)
        btnReset.frame = CGRect(x: 10, y: 350, width: self.view.frame.size.width - 20, height: 45)
        btnReset.setTitle("Reset", for: UIControlState.normal)
        btnReset.autoresizingMask = UIViewAutoresizing.flexibleWidth
        self.view.addSubview(btnReset)
        btnReset.addTarget(self, action: #selector(resetBtnTouched(btn:)), for: UIControlEvents.touchUpInside)
        
        
    }
    
    func team1StepperTouched(stepper:UIStepper) -> Void {
        lblTeam1.text = String(format: "%.f", stepper.value)
    }
    
    
    func team2StepperTouched(stepper:UIStepper) -> Void {
        lblTeam2.text = String(format: "%.f", stepper.value)
    }
    
    func resetBtnTouched(btn:UIButton) -> Void {
        
        stepperTeam1.value = 0
        stepperTeam2.value = 0
        
        lblTeam1.text = String(format: "%.f", stepperTeam1.value)
        lblTeam2.text = String(format: "%.f", stepperTeam2.value)
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

